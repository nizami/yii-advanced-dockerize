#!/bin/sh

#Запускаем инструкции докера
docker-compose build --force-rm
docker-compose up -d --force-recreate php
docker exec -it yii-advanced_php composer create-project --prefer-dist yiisoft/yii2-app-advanced app
docker-compose up -d --force-recreate backend frontend
docker exec -it yii-advanced_php ./app/init --env=Development --overwrite=All
